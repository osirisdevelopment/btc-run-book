'use strict'
const ActionHero = require('actionhero')
const Joi = require('joi')
const _ = require('lodash')
const binanceApi = require('node-binance-api')

const regexBTC = /(\w+)BTC$/
const Entry = Joi.object().pattern(/\d+\.\d+/, Joi.number().min(0))
const OrderBookModel = Joi.object({
  asks: Entry,
  bids: Entry
})

function transformOrderBook (book) {
  return new Map(_(book).toPairs().map(i => [Number(i[0]), i[1]]))
}

function transformOrderBookResponse (response) {
  const model = OrderBookModel.validate(response)

  return model.error === null ? {
    buy: transformOrderBook(model.value.bids),
    sell: transformOrderBook(model.value.asks),
    success: true
  } : {
    success: false
  }
}

function transformMarkets (response) {
  return _.reduce(response, (accum, item, currency) => {
    const match = regexBTC.exec(currency)
    if (match) {
      return accum.set(match[1], {
        currency: match[1],
        name: match[1],
        market: currency
      })
    }
    return accum
  }, new Map())
}

module.exports = class BinanceInitializer extends ActionHero.Initializer {
  constructor () {
    super()
    this.name = 'binance'
    this.loadPriority = 1000
    this.startPriority = 1000
    this.stopPriority = 1000
    this.binance = binanceApi
  }

  async initialize () {
    ActionHero.api.binance = {
      getOrderBook: async (market) => {
        return new Promise((resolve, reject) => this.binance.depth(market, (err, response) => {
          if (err) {
            reject(err)
          } else {
            resolve(transformOrderBookResponse(response))
          }
        }))
      },
      getMarkets: async () => {
        return new Promise((resolve, reject) => this.binance.prices((err, prices) => {
          if (err) {
            reject(err)
          } else {
            resolve(transformMarkets(prices))
          }
        }))
      }
    }

    return ActionHero.api.binance
  }

  async start () {
  }

  async stop () {
  }
}
