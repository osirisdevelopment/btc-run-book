'use strict'
const ActionHero = require('actionhero')
const Joi = require('joi')
const _ = require('lodash')
const Poloniex = require('poloniex-api-node')

const Entry = Joi.array().ordered(Joi.number().min(0).required(), Joi.number().min(0).required())
const OrderBookModel = Joi.object({
  asks: Joi.array().items(Entry).min(1),
  bids: Joi.array().items(Entry).min(1),
  isFrozen: Joi.boolean().truthy('1').falsy('0'),
  seq: Joi.number()
})

function transformOrderBook (book) {
  return new Map(_.map(book, (item) => [Number(item[0]), Number(item[1])]))
}

function transformOrderBookResponse (response) {
  const model = OrderBookModel.validate(response)

  return model.error === null ? {
    buy: transformOrderBook(model.value.bids),
    sell: transformOrderBook(model.value.asks),
    success: true
  } : {
    success: false
  }
}

function transformMarkets (response) {
  return _.reduce(response, (accum, item, currency) => {
    if (item.disabled === 0 && item.delisted === 0) {
      return accum.set(currency, {
        currency,
        name: item.name,
        market: `BTC_${currency}`
      })
    }
    return accum
  }, new Map())
}

module.exports = class PoloniexInitializer extends ActionHero.Initializer {
  constructor () {
    super()
    this.name = 'poloniex'
    this.loadPriority = 1000
    this.startPriority = 1000
    this.stopPriority = 1000
    this.poloniex = new Poloniex()
  }

  async initialize () {
    ActionHero.api.poloniex = {
      getOrderBook: async (market, depth) => {
        return transformOrderBookResponse(await this.poloniex.returnOrderBook(market, depth))
      },
      getMarkets: async () => {
        return transformMarkets(await this.poloniex.returnCurrencies())
      }
    }

    return ActionHero.api.poloniex
  }

  async start () {
  }

  async stop () {
  }
}
