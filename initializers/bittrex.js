'use strict'
const ActionHero = require('actionhero')
const Joi = require('joi')
const _ = require('lodash')
const bittrexApi = require('node-bittrex-api')

function CreateProxy (S) {
  return new Proxy(S, {
    get (instance, key) {
      const original = instance[key]
      if (typeof original === 'function') {
        return (...args2) => new Promise((resolve, reject) => {
          const cb = (data, err) => err ? reject(err) : resolve(data)
          args2.push(cb)
          Reflect.apply(original, instance, args2)
        })
      }
      return Reflect.get(instance, key)
    }
  })
}

const OrderBookModel = Joi.object({
  success: Joi.boolean().required(),
  message: Joi.string().allow(''),
  result: Joi.object({
    buy: Joi.array().items(Joi.object({
      Quantity: Joi.number().min(0).required(),
      Rate: Joi.number().min(0).required()
    })).min(1),
    sell: Joi.array().items(Joi.object({
      Quantity: Joi.number().min(0).required(),
      Rate: Joi.number().min(0).required()
    })).min(1)
  })
})

function transformOrderBook (book) {
  return new Map(_.map(book, (item) => [item.Rate, item.Quantity]))
}

function transformOrderBookResponse (response) {
  const model = OrderBookModel.validate(response)

  return model.error === null ? {
    buy: transformOrderBook(model.value.result.buy),
    sell: transformOrderBook(model.value.result.sell),
    success: model.value.success
  } : {
    success: false
  }
}

function transformMarkets (response) {
  return _.reduce(response.result, (accum, item) => {
    if (item.BaseCurrency === 'BTC') {
      return accum.set(item.MarketCurrency, {
        currency: item.MarketCurrency,
        name: item.MarketCurrencyLong,
        market: item.MarketName
      })
    }
    return accum
  }, new Map())
}

module.exports = class BittrexInitializer extends ActionHero.Initializer {
  constructor () {
    super()
    this.name = 'bittrex'
    this.loadPriority = 1000
    this.startPriority = 1000
    this.stopPriority = 1000
    this.bittrex = new CreateProxy(bittrexApi)
  }

  async initialize () {
    ActionHero.api.bittrex = {
      getOrderBook: async (market, depth) => {
        return transformOrderBookResponse(await this.bittrex.getorderbook({ market, depth, type: 'both' }))
      },
      getMarkets: async () => {
        return transformMarkets(await this.bittrex.getmarkets())
      }
    }

    return ActionHero.api.bittrex
  }

  async start () {
  }

  async stop () {
  }
}
