# ShapeShift BTC Run Book

## To install:
(assuming you have [node](http://nodejs.org/) and NPM installed)

`npm install`

## To Run:
`npm start`

## To Test:
`npm test`


# Requirements:

Build a combined order book that takes the full order books from Bittrex and Poloniex for the BTC_ETH
market and displays them in a way that shows combined volume at each price point.

## Please complete the challenge in Node.js
[X] Clearly label which is ask book and which is the bids book
[ ] Clearly label which exchange has order volume at each price point
[X] Please host a demo of the code running online (free heroku site or similar)

## Extra points for:
[X] More exchanges
[X] Allow user to switch to different markets (the BTC_DOGE market for example)
[X] Highlight if the books overlap (ie bids on Bittrex overlap with asks on Poloniex)
[ ] Actively updating the combined order book based on actual trades from the exchanges
[X] Unit Tests
