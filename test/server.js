'use strict'

const test = require('ava')

const ActionHero = require('actionhero')
const actionhero = new ActionHero.Process()
let api

test.before(async () => {
  api = await actionhero.start()
})

test.after(async () => {
  await actionhero.stop()
})

test('should have booted into the test env', (t) => {
  t.is(process.env.NODE_ENV, 'test')
  t.is(api.env, 'test')
  t.truthy(api.id)
})

test('can retrieve server uptime via the status action', async (t) => {
  let { uptime } = await api.specHelper.runAction('status')
  t.true(uptime > 0)
})
