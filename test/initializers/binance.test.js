'use strict'

const test = require('ava')
const Joi = require('joi')

const BinanceInitializer = require('../../initializers/binance')

const OrderBookModel = Joi.object({
  buy: Joi.object(),
  sell: Joi.object(),
  success: Joi.boolean().required()
})

test('should initialize', async (t) => {
  try {
    const b = new BinanceInitializer()
    t.true(b instanceof BinanceInitializer)
    const x = await b.initialize()
    t.truthy(x)
    t.is(typeof x.getOrderBook, 'function')
  } catch (e) {
    t.is(e, undefined, e.message)
  }
})

test('can get the order book', async (t) => {
  try {
    const b = await new BinanceInitializer().initialize()
    const book = await b.getOrderBook('ETHBTC')
    t.true(book.success)
    t.is(OrderBookModel.validate(book).error, null)
  } catch (e) {
    t.is(e, undefined, e.message)
  }
})

test('can get the available markets', async (t) => {
  try {
    const b = await new BinanceInitializer().initialize()
    const markets = await b.getMarkets()
    t.deepEqual(markets.get('ETH'), {
      currency: 'ETH',
      market: 'ETHBTC',
      name: 'ETH'
    })
  } catch (e) {
    t.is(e, undefined, e.message)
  }
})

test('can get the order book for all available markets', async (t) => {
  try {
    const b = await new BinanceInitializer().initialize()
    const markets = await b.getMarkets()
    const books = []
    // eslint-disable-next-line no-unused-vars
    for (const [currency, {market}] of markets) {
      books.push(b.getOrderBook(market, 10))
    }
    for (const book of await Promise.all(books)) {
      t.is(OrderBookModel.validate(book).error, null)
    }
  } catch (e) {
    t.is(e, undefined, e.message)
  }
})

// @TODO: Use nock to mock HTTP requests to return fixed data
