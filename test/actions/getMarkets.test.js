'use strict'

const ActionHero = require('actionhero')
const actionhero = new ActionHero.Process()
const test = require('ava')
const Joi = require('joi')

const MarketsModel = Joi.object().pattern(/[0-9A-Z]{2,4}/, Joi.object({
  currency: Joi.string().min(2).required(),
  name: Joi.string().min(2).required(),
  binance: Joi.string().optional(),
  bittrex: Joi.string().optional(),
  poloniex: Joi.string().optional()
}))

let api
test.before(async () => {
  api = await actionhero.start()
})

test.after(async () => {
  await actionhero.stop()
})

test('can get combined markets', async (t) => {
  try {
    const markets = await api.specHelper.runAction('getMarkets')
    t.is(MarketsModel.validate(markets.data).error, null)
  } catch (e) {
    t.is(e, undefined, e.message)
  }
})

// @TODO: Use nock to mock HTTP requests to return fixed data
