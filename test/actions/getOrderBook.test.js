'use strict'

const ActionHero = require('actionhero')
const actionhero = new ActionHero.Process()
const test = require('ava')
const Joi = require('joi')

const EntryModel = Joi.array().ordered(Joi.number().min(0).required(), Joi.object({
  quantity: Joi.number().min(0).required(),
  exchanges: Joi.array()
    .items(Joi.string().valid(['bittrex', 'binance', 'poloniex'])).required()
}))

const OrderBookModel = Joi.object({
  buy: Joi.array().items(EntryModel),
  sell: Joi.array().items(EntryModel)
})

let api
test.before(async () => {
  api = await actionhero.start()
})

test.after(async () => {
  await actionhero.stop()
})

test('can get combined markets for the default currency and Litecoin', async (t) => {
  try {
    const defaultBook = await api.specHelper.runAction('getOrderBook')
    const LTCbook = await api.specHelper.runAction('getOrderBook', { market: 'LTC' })
    // Parse from JSON to simulate the API response
    const response = JSON.parse(JSON.stringify(LTCbook))
    const defaultResponse = JSON.parse(JSON.stringify(defaultBook))

    t.is(OrderBookModel.validate(response.data).error, null)
    t.notDeepEqual(defaultResponse.data, response.data)
  } catch (e) {
    t.is(e, undefined, e.message)
  }
})

// @TODO: Use nock to mock HTTP requests to return fixed data
