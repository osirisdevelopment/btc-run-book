'use strict'
const _ = require('lodash')
const ActionHero = require('actionhero')

function combineMarkets (data) {
  const combined = {}

  _.forEach(data, (market, name) => {
    if (market) {
      for (const [key, value] of market) {
        const current = combined[key] || {
          currency: value.currency,
          name: value.name
        }
        current[name] = value.market
        combined[value.currency] = current
      }
    }
  })
  return combined
}

const getMarkets = _.memoize(async () => {
  const { bittrex, poloniex, binance } = ActionHero.api
  const markets = await Promise.all([
    bittrex.getMarkets(),
    poloniex.getMarkets(),
    binance.getMarkets()
  ])

  return combineMarkets({
    bittrex: markets[0],
    poloniex: markets[1],
    binance: markets[2]
  })
})

module.exports = class GetMarkets extends ActionHero.Action {
  constructor () {
    super()
    this.name = 'getMarkets'
    this.description = 'Get Markets'
  }

  async run ({ response }) {
    response.data = await getMarkets()
  }
}
