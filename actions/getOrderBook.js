'use strict'
const _ = require('lodash')
const ActionHero = require('actionhero')

/* eslint-disable no-extend-native */
Map.prototype.toJSON = function toJSON () {
  return _.sortBy(_.toPairs(this), i => -i[0])
}

Set.prototype.toJSON = function toJSON () {
  return Array.from(this)
}

function combineOrderBooks (data) {
  const combined = {
    buy: new Map(),
    sell: new Map()
  }

  _(data).forEach((exchange, name) => {
    if (exchange && exchange.buy) {
      _.forEach(['buy', 'sell'], book => {
        for (const [price, quantity] of exchange[book]) {
          if (typeof price === 'number' && typeof quantity === 'number') {
            const current = combined[book].get(price) || { quantity: 0, exchanges: new Set() }
            current.quantity += quantity
            current.exchanges.add(name)
            combined[book].set(price, current)
          }
        }
      })
    }
  })

  return combined
}

module.exports = class GetOrderBook extends ActionHero.Action {
  constructor () {
    super()
    this.name = 'getOrderBook'
    this.description = 'Get Order Book'
    this.inputs = {
      market: {
        required: true
      }
    }
  }

  async run ({params, response}) {
    const {bittrex, poloniex, binance} = ActionHero.api
    const markets = {}
    await ActionHero.api.actions.actions.getMarkets[1].run({ response: markets })

    const market = markets.data[params.market]
    if (market) {
      const mBittrex = markets.data[params.market].bittrex
      const mPoloniex = markets.data[params.market].poloniex
      const mBinance = markets.data[params.market].binance
      const books = await Promise.all([
        mBittrex ? bittrex.getOrderBook(mBittrex, 100) : {},
        mPoloniex ? poloniex.getOrderBook(mPoloniex, 100) : {},
        mBinance ? binance.getOrderBook(mBinance, 100) : {}
      ])

      response.data = combineOrderBooks({
        bittrex: books[0],
        poloniex: books[1],
        binance: books[2]
      })
    } else {
      throw new Error('Invalid market name')
    }
  }
}
